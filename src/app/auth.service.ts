import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {AngularFireDatabase,AngularFireList} from '@angular/fire/database';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {
 
  signup(email:string,password:string){
    return this.fireBaseAuth.auth.createUserWithEmailAndPassword(email,password);
    }
    
    updateProfile(user,name:string){
      user.updateProfile({displayName:name,photoURL:''});
    }

 
    user: Observable<firebase.User>;
    constructor(private fireBaseAuth:AngularFireAuth) {
      this.user = fireBaseAuth.authState; }
}
